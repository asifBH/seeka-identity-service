package com.indentity.dao;

import com.identity.model.User;

public interface UserDao {

	public void saveUser(User user);

	public User findByEmail(String emailAddress);
}
