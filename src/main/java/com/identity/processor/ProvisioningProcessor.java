package com.identity.processor;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.identity.exception.SeekaException;
import com.identity.handler.RegistrationEmailHandler;
import com.identity.handler.UserProvisioningHandler;
import com.identity.utils.SecurityUtils;

@Component
public class ProvisioningProcessor {

	@Autowired
	private UserProvisioningHandler userProvisioningHandler;
	
	@Autowired
	private RegistrationEmailHandler registrationEmailHandler;
	
	@Autowired
	private SecurityUtils securityUtils;

	public void provisionUser(String firstName, String lastName, String email, String phoneNumber, String userName,
			String password) {
		// Encryting email Address as its not recommended to store password in plain text
		String encryptedEmailAddress = securityUtils.encrypt(email);
		// checking if user already exist for given email address
		boolean userExist = userProvisioningHandler.checkIfUserExist(encryptedEmailAddress);
		// if User already exsist throw unauthorized exception
		if (userExist) {
			throw new SeekaException("SIS4001", "User exist with given email address",
					"User exist with given email address please sign in to continue or try registering with different email address", HttpStatus.UNAUTHORIZED.value());
		}
		// Generating account_guid which will be identity of user in our system
		String accountGuid = UUID.randomUUID().toString();
		// creating one way hash out of password as its not recommended save password even in encrypted form
		String passwordDigest = securityUtils.getPasswordDigest(password);
		userProvisioningHandler.saveUserModel(firstName,lastName,encryptedEmailAddress,phoneNumber,userName,passwordDigest,accountGuid);
		// calling email-notification-service for sending email to register user
		try {
			registrationEmailHandler.sendRegistrationEmail(encryptedEmailAddress);
		} catch (Exception e) {
			//here we can write retry logic if our first reqest to email-notification-service fail
			throw e;
		}
		
		
	}
}
