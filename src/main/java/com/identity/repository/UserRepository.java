package com.identity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.identity.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{

	/**
	 * This method will check in DB if we have user with given email address
	 * We dont need to write implementation of this method as JpaRepository have feature where it will call DB using method name 
	 * 
	 * Ref:- https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods
	 * 
	 * @param email
	 * @return
	 */
	public User findByEmail(String email);
	
}
