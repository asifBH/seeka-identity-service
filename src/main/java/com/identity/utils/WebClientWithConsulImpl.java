package com.identity.utils;

import java.net.URI;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.util.Assert;

public class WebClientWithConsulImpl {

	private String serviceName = null;
	private  LoadBalancerClient loadBalancerClient;
	private  Client client;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public LoadBalancerClient getLoadBalancerClient() {
		return loadBalancerClient;
	}

	@Autowired
	public void setLoadBalancerClient(LoadBalancerClient loadBalancerClient) {
		this.loadBalancerClient = loadBalancerClient;
	}

	public Client getClient() {
		return client;
	}

	@Autowired
	public void setClient(Client client) {
		this.client = client;
	}

	private URI buildUrl() {
		if (this.serviceName == null) {
			throw new NullPointerException(
					"cannot call service because service name is not set. Typically you assign a service name in your spring config");
		}
		try {
			ServiceInstance serviceInstance = loadBalancerClient.choose(this.serviceName);
			Assert.notNull(serviceInstance, this.serviceName + " not found");
			return serviceInstance.getUri();
		} catch (Exception e) {
			throw new RuntimeException("Exception caught obtaining url for service [" + this.serviceName + "]", e);
		}
	}
	
	public WebTarget getWebTarget() {
		WebTarget webTarget = client.target(buildUrl());
		return webTarget;
	}
}
