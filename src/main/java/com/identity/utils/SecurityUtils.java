package com.identity.utils;

import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.identity.exception.SeekaException;

@Service
public class SecurityUtils {

	// Here I have stored all value as final string intead of reading it from properties file because this value should never change once platform is deployed
	private static final String encryptionKey = "ASNZBHQWDFVCAWAQ";
	private static final String characterEncoding = "UTF-8";
	private static final String cipherTransformation = "AES/CBC/PKCS5PADDING";
	private static final String aesEncryptionAlgorithem = "AES";
	private static final String salt = "xBpbVpDXRHfc7okN";
	private static final String iterations = "10000";
	private static final String keyLength = "512";
	private static final String hashAlogorithm = "PBKDF2WithHmacSHA512";

	/**
	 * Method for Encrypt Plain String Data
	 * 
	 * @param plainText
	 * @return encryptedText
	 */
	public String encrypt(String plainText) {
		String encryptedText = "";
		try {
			Cipher cipher = Cipher.getInstance(cipherTransformation);
			byte[] key = encryptionKey.getBytes(characterEncoding);
			SecretKeySpec secretKey = new SecretKeySpec(key, aesEncryptionAlgorithem);
			IvParameterSpec ivparameterspec = new IvParameterSpec(key);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, ivparameterspec);
			byte[] cipherText = cipher.doFinal(plainText.getBytes("UTF8"));
			Base64.Encoder encoder = Base64.getEncoder();
			encryptedText = encoder.encodeToString(cipherText);

		} catch (Exception E) {
			throw new SeekaException("SIS5002", "Error while encryting email address",
					"Error while encryting email address", HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return encryptedText;
	}

	
	private byte[] hashPassword(String password) {

		char[] passwordChars = password.toCharArray();
		byte[] saltBytes = salt.getBytes();
		byte[] res = null;
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance(hashAlogorithm);
			PBEKeySpec spec = new PBEKeySpec(passwordChars, saltBytes, Integer.parseInt(iterations),
					Integer.parseInt(keyLength));
			SecretKey key = skf.generateSecret(spec);
			res = key.getEncoded();
		} catch (Exception e) {
			throw new SeekaException("SIS5002", "Error while generating hash password",
					"Error while generating hash password", HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		return res;
	}

	private StringBuffer byteToHex(byte[] value) {

		StringBuffer hexValue = new StringBuffer();
		for (int i = 0; i < value.length; i++) {
			hexValue.append(Integer.toString((value[i] & 0xff) + 0x100, 16).substring(1));
		}
		return hexValue;
	}
	
	/**
	 * Method for Hash Plain Text Password using PBKDF2WithHmacSHA512 Algo 
	 * 
	 * @param plainText
	 * @return hashedPassword
	 */
	public String getPasswordDigest(String plainTextPassword) {
		byte[] hashedPassword = hashPassword(plainTextPassword);
		return byteToHex(hashedPassword).toString();
	}
	
}
