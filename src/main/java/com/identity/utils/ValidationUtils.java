package com.identity.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Service;

@Service
public class ValidationUtils {

	private static final String EMAIL_REGEX = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";

	private static Pattern pattern;

	/**
	 * This method check supplied email is according to standard format
	 * 
	 * @param emailAddress
	 * @return boolean which confirm if email is valid
	 */
	public static boolean emailValidator(String emailAddress) {
		pattern = Pattern.compile(EMAIL_REGEX);
		Matcher matcher = pattern.matcher(emailAddress);
		return matcher.matches();
	}
}
