package com.identity.endpoint;

import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.identity.dto.UserRegistration;

@Path("/seeka/api/v1")
public interface ProvisioningInterface {

	@POST
	@Path("/register")
	@Consumes({ "application/json", "application/xml" })
	@Produces({ "application/json", "application/xml" })
	public void registerUser(@Valid UserRegistration userRegistration);
	
}
