package com.identity.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.identity.model.User;
import com.indentity.dao.UserDao;

@Service
public class UserProvisioningHandler {

	@Autowired
	private UserDao userDao;

	public boolean checkIfUserExist(String emailAddress) {
		User user = userDao.findByEmail(emailAddress);
		if (ObjectUtils.isEmpty(user)) {
			return false;
		} else {
			return true;
		}
	}

	public void saveUserModel(String firstName, String lastName, String encryptedEmailAddress, String phoneNumber,
			String userName, String passwordDigest, String accountGuid) {
		User user = new User(firstName, lastName, accountGuid, passwordDigest, encryptedEmailAddress, userName,
				phoneNumber);
		userDao.saveUser(user);
	}
}
