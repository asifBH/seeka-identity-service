package com.identity.handler;

import javax.annotation.Resource;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.identity.exception.ErrorWrapper;
import com.identity.exception.SeekaException;
import com.identity.utils.WebClientWithConsulImpl;

@Service
public class RegistrationEmailHandler {

	private static final String EMAIL_NOTIFICATION_FAILED_MESSAGE = "Call to email notification has been failed.";
	private static final String EMAIL_NOTIFICATION_FAILED_DETAIL = "operation by email notification has been failed.";
	
	@Resource(name = "emailNotificationWebClient")
	private WebClientWithConsulImpl emailNotificationWebClient;

	@Value("${path.email.notification.api}")
	private String pathEmailNotificationApi;

	// This method will call emailNotificationService using Jersey web client. There would be single instance fro jersey web client
	// for email notification service also I have implemented spring boot LoadBalancer and Service Discover via consul
	public void sendRegistrationEmail(String emailAddress) {
		StringBuilder pathEmailNotification = new StringBuilder(pathEmailNotificationApi);
		Response response = null;
		try {
			response = emailNotificationWebClient.getWebTarget().path(pathEmailNotification.toString())
					.queryParam("email", emailAddress).request().header("Content-Type", MediaType.APPLICATION_JSON)
					.accept(MediaType.APPLICATION_JSON).get();
			
			if (response.getStatus() == 404 || response.getStatus() == 500) {
				ErrorWrapper error = new ErrorWrapper();
				error.setErrorCode("SIS5003");
				error.setErrorMessage(EMAIL_NOTIFICATION_FAILED_MESSAGE);
				error.setErrorDetail(EMAIL_NOTIFICATION_FAILED_DETAIL);
				throw new SeekaException(HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
			}
			
		} catch (Exception e) {
			System.err.println(e);
			if (e instanceof SeekaException) {
				throw e;
			} else {
				ErrorWrapper error = new ErrorWrapper();
				error.setErrorCode("SIS5003");
				error.setErrorMessage(EMAIL_NOTIFICATION_FAILED_MESSAGE);
				error.setErrorDetail(EMAIL_NOTIFICATION_FAILED_DETAIL);
				throw new SeekaException(HttpStatus.INTERNAL_SERVER_ERROR.value(), error);
			}
		} finally {
			if (response != null) {
				response.close();
			}
		}
	}
}
