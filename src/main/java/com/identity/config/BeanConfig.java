package com.identity.config;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.ClientProperties;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.identity.utils.WebClientWithConsulImpl;

@Configuration
public class BeanConfig {
	
	@Value("${email.notification.service.name}")
	private String emailNotificationServiceName;
	
	/**
	 * Create Jersey Web Client which is injected in WebClientWithConsulImpl
	 * @return Jersey Web Client
	 */
	@Bean
	public Client createClient() {
		Client client = null;
		ClientConfig clientConfig = new ClientConfig();
		// values are in milliseconds
		clientConfig.property(ClientProperties.READ_TIMEOUT, 20000);
		clientConfig.property(ClientProperties.CONNECT_TIMEOUT, 10000);
		client = ClientBuilder.newClient(clientConfig);
		return client;
	}

	/**
	 * Create web client impl with consul for email-notification-service
	 * @return EmailWebClient
	 */
	@Bean("emailNotificationWebClient")
	public WebClientWithConsulImpl emailNotificationWebClient () {
		WebClientWithConsulImpl webClientWithConsulImpl = new WebClientWithConsulImpl();
		webClientWithConsulImpl.setServiceName(emailNotificationServiceName);
		return webClientWithConsulImpl;
	}
}
