package com.identity.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.identity.model.User;
import com.identity.repository.UserRepository;
import com.indentity.dao.UserDao;

@Component
public class UserDaoImpl implements UserDao {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public void saveUser(User user) {
		userRepository.save(user);
	}

	@Override
	public User findByEmail(String emailAddress) {
		return userRepository.findByEmail(emailAddress);
	}

}
