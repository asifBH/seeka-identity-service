package com.identity.exception;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "error_wrapper")
public class ErrorWrapper {

	@XmlElement(name = "error_code")
	private String errorCode = null;
	@XmlElement(name = "error_message")
	private String errorMessage = null;
	@XmlElement(name = "error_detail")
	private String errorDetail = null;
	
	public ErrorWrapper() {}

	public ErrorWrapper(String errorCode, String errorMessage, String errorDetail) {
		super();
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
		this.errorDetail = errorDetail;
	}

	/**
	 * Error code
	 * 
	 * @return errorCode
	 **/
	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public ErrorWrapper errorCode(String errorCode) {
		this.errorCode = errorCode;
		return this;
	}

	/**
	 * Error message
	 * 
	 * @return errorMessage
	 **/
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ErrorWrapper errorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
		return this;
	}

	/**
	 * Error detail
	 * 
	 * @return errorDetail
	 **/
	public String getErrorDetail() {
		return errorDetail;
	}

	public void setErrorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
	}

	public ErrorWrapper errorDetail(String errorDetail) {
		this.errorDetail = errorDetail;
		return this;
	}

	@Override
	public String toString() {
		 StringBuffer sb = new StringBuffer(1024);
	      sb.append("error_code: ");
	      sb.append(this.errorCode);
	      sb.append(", error_message: " );
	      sb.append(this.errorMessage);
	      sb.append(", error_detail: ");
	      sb.append(this.errorDetail);
	    return sb.toString();
	}
}