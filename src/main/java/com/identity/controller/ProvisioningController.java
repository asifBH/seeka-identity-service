package com.identity.controller;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.RestController;

import com.identity.dto.UserRegistration;
import com.identity.endpoint.ProvisioningInterface;
import com.identity.exception.SeekaException;
import com.identity.processor.ProvisioningProcessor;
import com.identity.utils.ValidationUtils;

@RestController
public class ProvisioningController implements ProvisioningInterface  {

	@Autowired
	private ProvisioningProcessor provisioningProcessor;
	
	@Override
	public void registerUser(@Valid UserRegistration userRegistration) {
		try {
			if (ObjectUtils.isEmpty(userRegistration)) {
				throw new SeekaException("SIS4001", "Please pass request body", "Please pass valid request body",
						HttpStatus.BAD_REQUEST.value());
			}

			if (StringUtils.isBlank(userRegistration.getFirstName())) {
				throw new SeekaException("SIS4002", "First Name not found", "First Name is Null or Empty",
						HttpStatus.BAD_REQUEST.value());
			}

			if (StringUtils.isBlank(userRegistration.getLastName())) {
				throw new SeekaException("SIS4003", "Last Name not found", "Last Name is Null or Empty",
						HttpStatus.BAD_REQUEST.value());
			}

			if (StringUtils.isBlank(userRegistration.getEmail())) {
				throw new SeekaException("SIS4004", "Email Id not found", "Email ID is Null or Empty",
						HttpStatus.BAD_REQUEST.value());
			}

			if (!ValidationUtils.emailValidator(userRegistration.getEmail().trim())) {
				throw new SeekaException("SIS4005", "Please enter valid email address", "Please enter valid email address",
						HttpStatus.BAD_REQUEST.value());
			}
			
			if (StringUtils.isBlank(userRegistration.getPhoneNumber())) {
				throw new SeekaException("SIS4006", "Phone number not found", "Phone number is Null or Empty",
						HttpStatus.BAD_REQUEST.value());
			}
			
			if (StringUtils.isBlank(userRegistration.getUserName())) {
				throw new SeekaException("SIS4007", "User name not found", "User name is Null or Empty",
						HttpStatus.BAD_REQUEST.value());
			}
			
			if (StringUtils.isBlank(userRegistration.getPassword())) {
				throw new SeekaException("SIS4008", "Password name not found", "Password is Null or Empty",
						HttpStatus.BAD_REQUEST.value());
			}
			
			provisioningProcessor.provisionUser(userRegistration.getFirstName(),userRegistration.getLastName(),userRegistration.getEmail(),userRegistration.getPhoneNumber(),userRegistration.getUserName(),userRegistration.getPassword());
		} catch (Exception e) {
			if (e instanceof SeekaException) {
				throw e;
			} else {
				throw new SeekaException("SIS5001", e.getMessage() != null ? e.getMessage() : "Internal Server Error",
						e.getCause() != null ? e.getCause().getMessage() : "Internal Server Error",
						HttpStatus.INTERNAL_SERVER_ERROR.value());
			}
		}
		
	}

}
